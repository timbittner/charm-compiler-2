"""This module parses the in memory representation into a dot graph file"""
# command line to compile png:$ dot -Tpng DotFiles\Example.dot -o DotFiles\Images\Example.png
import os
from IntermediateRepresentation.Representation import BasicBlock

DOT_FOLDER = "DotFiles/"
IMG_FOLDER = DOT_FOLDER + "Images/"
DRAW_DOM = True
PRINT_PHI_SYM = False


def dot_from_representation(root: BasicBlock, filename: str):

    printed_blocks = []
    dot_bbs: [str] = []
    dot_edges: [str] = []

    def print_instr(block):
        group_open = False
        ins = "{"
        for i, instr in enumerate(block.instructions):
            # check for array instruction, opening
            if instr.name == "adda":
                ins += "{  | {"
                group_open = True

            # special case: functions
            elif instr.name == "call":
                ins += f"<{instr.count}>"
                function_block = instr.function_block
                print_block(function_block)
                # branch edges have less weight so they can be longer
                dot_edge = f"bb{block.block_id}:{instr.count} -> bb{function_block.block_id}:n [color=darkgreen, weight=0 label=<<font color='darkgreen'>call</font>>];"
                dot_edges.append(dot_edge)

            ins += str(instr)

            if PRINT_PHI_SYM and instr.name == "Phi":
                ins += f" -- {instr.symbol}"

            # check for array instruction, ending
            if group_open and (instr.name == "load" or instr.name == "store"):
                ins += "} }"
                group_open = False

            # end of block
            if i < len(block.instructions)-1:
                ins += "|"
        ins += "}"
        return ins

    def print_block(b: BasicBlock):
        if b.block_id not in printed_blocks:
            # print bb and instructions
            dot_bb = f"bb{b.block_id} [shape=record, label=\"<b>BB{b.block_id}|{print_instr(b)}\"];"
            dot_bbs.append(dot_bb)
            printed_blocks.append(b.block_id)

            # print edges and next block
            if b.fallthrough is not None:
                dot_edge = f"bb{b.block_id}:s -> bb{b.fallthrough.block_id}:n [label=\"fall-through\"];"
                dot_edges.append(dot_edge)
                print_block(b.fallthrough)

            if b.branch is not None:
                # branch edges have less weight so they can be longer
                dot_edge = f"bb{b.block_id}:s -> bb{b.branch.block_id}:n [color=brown, weight=0 label=<<font color='brown'>branch</font>>];"
                dot_edges.append(dot_edge)
                print_block(b.branch)

            # print dominations
            if DRAW_DOM:
                for dominated in b.dominator_list:
                    dot_edge = f"bb{b.block_id}:b -> bb{dominated.block_id}:b [color=blue, style=dotted, label=<<font color='blue'>dom</font>>];"
                    dot_edges.append(dot_edge)

    # build dot file recursively
    print_block(root)

    # piece lists together
    dot_contents = "digraph G {\n"
    for bb in dot_bbs:
        dot_contents += bb + "\n"
    for e in dot_edges:
        dot_contents += e + "\n"
    dot_contents += "}"

    # write output
    dot_file = open(f"{DOT_FOLDER + filename}.dot", "w")
    dot_file.write(dot_contents)
    dot_file.close()

    # print("Generate PNG from DOT File")
    os.system(f"dot -Tpng {DOT_FOLDER + filename}.dot -o {IMG_FOLDER + filename}.png")

    # print("Output Generation finished")


if __name__ == "__main__":
    dot_from_representation(None, "Example")

