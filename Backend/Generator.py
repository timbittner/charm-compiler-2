""" Responsible for generating binary code"""


def put_f1(opcode, a, b, c):
    """Emits instructions for DXR Machine """
    print(f"F1  -  OP:{opcode} A:{a} B:{b} C:{c}")



def put_f2(opcode, a, b, c):
    """Emits instructions for DXR Machine """
    print(f"F2  -  OP:{opcode} A:{a} B:{b} C:{c}")


def put_f3(opcode, a, b, c):
    """Emits instructions for DXR Machine """
    print(f"F3  -  OP:{opcode} A:{a} B:{b} C:{c}")
