"""
Module Stolen from https://docs.python.org/3/library/re.html#writing-a-tokenizer
"""
from typing import NamedTuple
import re


class Token(NamedTuple):
    type: str
    value: str
    line: int
    column: int


def tokenize(code):
    keywords = {"let", "call", "if", "then", "else", "fi", "while", "do", "od", "return",
                "var", "array", "void", "function", "function", "main",
                }
    token_specification = [
        ('Ident', r"[a-zA-Z][a-zA-Z0-9]*"),
        ('Number', r"[0-9]+"),
        ('Special', r"\;|\.|\,|\<\-"),
        ('RelOp', r"\=\=|\>\=|\<\=|\!\=|\>|\<"),
        ('Operator', r"\+|\-|\*|\/"),
        ('Braces', r"\(|\)|\[|\]|\{|\}"),
        ('NEWLINE', r'\n'),  # Line endings
        ('SKIP', r'[ \t]+'),  # Skip over spaces and tabs
        ('MISMATCH', r'.'),  # Any other character
    ]
    tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
    line_num = 1
    line_start = 0
    token_stream = []
    for mo in re.finditer(tok_regex, code):
        kind = mo.lastgroup
        value = mo.group()
        column = mo.start() - line_start
        if kind == 'Number':
            int(value)
        elif kind == 'Ident' and value in keywords:
            kind = value
        elif kind == 'NEWLINE':
            line_start = mo.end()
            line_num += 1
            continue
        elif kind == 'SKIP':
            continue
        elif kind == 'MISMATCH':
            raise RuntimeError(f'{value!r} unexpected on line {line_num}')
        t = Token(kind, value, line_num, column)
        token_stream.append(t)
    return token_stream