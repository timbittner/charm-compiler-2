""" Entry point of compile process(es) """
import Tokenizer
from Backend.DotGen import dot_from_representation
from Parser.Parser import RecursiveDescentParser
import IntermediateRepresentation.Representation as ir


def start_compile():
    # load file
    filename = "Euclid"

    f = open(f"TestFiles/{filename}.charm", "r")
    content = f.read()
    f.close()

    # Tokenize
    tokens = Tokenizer.tokenize(content)
    # debug
    for token in tokens:
        print(f"{token.value} ", end="")
    print()

    # parse
    parser = RecursiveDescentParser(tokens)
    parser.parse()

    # print intermediate representation
    ir.print_repr()

    # generate output files
    dot_from_representation(ir.root, filename)


if __name__ == "__main__":
    start_compile()
