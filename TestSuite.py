from time import sleep

import Tokenizer
from Backend.DotGen import dot_from_representation
from Parser.Parser import RecursiveDescentParser
import IntermediateRepresentation.Representation as ir


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def start_tests():
    one_test("computation")
    one_test("ifOnly")
    one_test("ifEnhanced")
    one_test("ifNested")
    one_test("whileOnly")
    one_test("whileIf")
    one_test("ifWhile")
    one_test("whileNested")
    one_test("inbuilt")
    one_test("StepTwo")  # Copy propagation and common subexpression elimination for arithmetic
    one_test("CSEJan")  # Describes a single pass problem with CSE in while loops
    one_test("array")
    one_test("arrayMD")
    one_test("arrayConditions")
    one_test("arrayMany")

    one_test("functionVoid")
    one_test("funcVoidPara")
    one_test("funcReturn")
    one_test("funcReturnPara")
    one_test("funcArray")
    one_test("funcRecursion")

    one_test("EuclidMain")  # further tests of phi value propagation in loops
    one_test("Euclid")

def one_test(filename):
    # print("=============================================")
    ir.reset()

    print(f"Testcase {filename:>24}: ", end="")

    f_input = open(f"TestFiles/{filename}.charm", "r")
    content = f_input.read()
    f_input.close()
    tokens = Tokenizer.tokenize(content)
    parser = RecursiveDescentParser(tokens)
    parser.parse()
    dot_from_representation(ir.root, filename)

    # load reference file
    f_reference = open(f"TestFiles/ReferenceOutputs/{filename}.dot", "r")
    reference = f_reference.read()
    f_reference.close()

    # load output file
    f_output = open(f"DotFiles/{filename}.dot", "r")
    output = f_output.read()
    f_output.close()

    # compare file contents
    if output == reference:
        print (f"{bcolors.OKGREEN}Successful{bcolors.ENDC}")
    else:
        print(f"{bcolors.FAIL}Failed{bcolors.ENDC}")

    # wait a little for console to catch up
    sleep(0.150)


if __name__ == "__main__":
    start_tests()
