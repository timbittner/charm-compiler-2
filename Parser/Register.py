from Parser.Result import Result, ResultKind
import IntermediateRepresentation.Representation as ir


class RegisterManager:
    """ A class that holds an manages registers in memory while compiling and performs computations """
    def __init__(self):
        self.filled_registers = [False] * 32
        # init special registers 0, 29, 30, 31
        self.filled_registers[0] = True
        self.filled_registers[29] = True
        self.filled_registers[30] = True
        self.filled_registers[31] = True

    def allocate_register(self) -> int:
        """Allocates a free register. Raises runtime exception if there is none"""
        reg_num = -1
        for i, reg in enumerate(self.filled_registers):
            if reg is False:
                self.filled_registers[i] = True
                reg_num = i
                break
        if reg_num == -1:
            raise RuntimeError("No free registers!")
        else:
            return reg_num

    def deallocate_register(self, i: int):
        """Deallocates given register"""
        self.filled_registers[i] = False
        pass

    def load(self, x: Result):
        """ Loads into register """
        if x.kind == ResultKind.CONST:
            x.r = self.allocate_register()
            # add immediate with register 0
            ir.append_instruction("ADDI", x.r, 0, x.val)
            x.kind = ResultKind.REG
        elif x.kind == ResultKind.VAR:
            x.r = self.allocate_register()
            # LDW with rBase=30
            ir.append_instruction("load", x.addr)  # "LDW", x.r, 30, x.addr
            x.kind = ResultKind.REG

    def compute(self, op, x: Result, y: Result):
        """Expects op Code and two result instances"""
        if x.kind == y.kind == ResultKind.CONST:
            xv = int(x.val)
            yv = int(y.val)
            if op == "ADD":
                x.val = xv + yv  # addition
            elif op == "SUB":
                x.val = xv - yv  # subtraction
            elif op == "MUL":
                x.val = xv * yv  # multiplication
            elif op == "DIV":
                x.val = xv // yv  # division
        else:
            # load x into register
            self.load(x)
            if y.kind == ResultKind.CONST:
                ir.append_instruction(op + "I", x.r, x.r, y.val)
            else:
                # load y into register
                self.load(y)
                ir.append_instruction(op, x.r, x.r, y.r)
                self.deallocate_register(y.r)

    def compare(self, x: Result, y: Result):
        # load a new register for comparison result
        compare_result = Result()
        compare_result.kind = ResultKind.REG
        compare_result.r = self.allocate_register()
        if x.kind == y.kind == ResultKind.CONST:
            # todo optimize: eliminate branch
            ir.append_instruction("CMP", compare_result.r, x.val, y.val)
        else:
            # load x into register
            self.load(x)  # todo dealloc?
            if y.kind == ResultKind.CONST:
                ir.append_instruction("CMP", compare_result.r, x.r, y.val)
            else:
                # load y into register
                self.load(y)
                ir.append_instruction("CMP", compare_result.r, x.r, y.r)
                self.deallocate_register(y.r)
        return compare_result






