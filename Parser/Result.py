from enum import Enum


class ResultKind(Enum):
    CONST = 1
    VAR = 2
    REG = 3


class Result:
    """ Result Datastructure """
    kind: ResultKind  # const, var reg
    val: int  # if const
    addr: int  # if var
    r: int  # if register
