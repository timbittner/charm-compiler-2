"""
recursive descent parser
"""
from enum import Enum
import IntermediateRepresentation.Representation as ir
from Tokenizer import Token


class ComputationType(Enum):
    VAR = 0
    LITERAL = 1


class Computation:
    def __init__(self, kind: ComputationType = None, value: int = None):
        self.type = kind if kind is not None else ComputationType.VAR
        self.value: int = value if value is not None else -9999  # integer representing instruction counter or literal

    def __str__(self):
        if self.type == ComputationType.VAR:
            return f"({self.value})"
        else:
            return f"#{self.value}"


class RecursiveDescentParser:
    def __init__(self, tokens):
        self.tokens: [Token] = tokens
        self.index = 0

    def tok(self) -> Token:
        """
        :return: The current token being processed
        """
        return self.tokens[self.index]

    def parse(self):
        if self.tok().value == 'main':
            self.index += 1
            self.main()
        else:
            self.error("File not starting with main")

    def error(self, message=""):
        """ Errors out at the current token adding user defined messages"""
        raise RuntimeError(f'{self.tok().value!r} '
                           f'unexpected on line {self.tok().line} '
                           f'column {self.tok().column}.\n'
                           + message)

    def main(self):

        self.type_decl()

        # Variable/Array Declaration
        while self.tok().type == "var" or self.tok().type == "array":
            if self.tok().type == "var":
                # variable declarations
                self.var_decl()
            else:
                # array declaration
                self.arr_decl()

        # Function declaration
        while self.tok().type == "function" or self.tok().type == "void":
            # function declaration
            self.func_decl()

        # Main Body
        if self.tok().value == "{":
            # body
            self.index += 1
            self.stat_sequence(None)
        else:
            self.error("Expected Body.")

        # Done with main body
        if self.tok().value == "}":
            self.index += 1
            if self.tok().value == ".":
                self.index += 1
                ir.append_instruction("end")
            else:
                self.error("Expected ending '.'")
        else:
            self.error("Expected ending '}'")

    def type_decl(self):
        # Variable/Array Declaration
        while self.tok().type == "var" or self.tok().type == "array":
            if self.tok().type == "var":
                # variable declarations
                self.var_decl()
            else:
                # array declaration
                self.arr_decl()

    def var_decl(self):
        """ Called if var keyword is token, stops once ; is parsed
        """
        self.index += 1
        if self.tok().type != "Ident":
            self.error("Expected Identifier")
        while self.tok().type == "Ident":
            # insert variables with their name as value because they are not assigned yet and might be params
            ir.current_bb.symbol_table.insert(self.tok().value, self.tok().value)
            self.index += 1
            if self.tok().value == ",":
                # handle another variable
                self.index += 1
            elif self.tok().value == ";":
                # end of var declaration
                self.index += 1
                # break
            else:
                self.error("Expected ',' or ';' in var declaration")

    def arr_decl(self):
        """ Called if arr keyword is token, stops once ; is parsed"""
        self.index += 1

        # define array dimensions
        if self.tok().value != "[":
            self.error("Tried to declare array with 0 dimensions.")

        dimensions = []
        while self.tok().value == "[":
            self.index += 1
            if self.tok().type == "Number":
                dimensions.append(int(self.tok().value))
                self.index += 1
            else:
                self.error("Error Reading Array Dimension")
            if self.tok().value == "]":
                self.index += 1
            else:
                self.error("Missing Closing ']' bracket in array declaration")

        # identifiers follow that will be arrays of dimensions
        if self.tok().type != "Ident":
            self.error("Expected Identifier")

        while self.tok().type == "Ident":
            ident = self.tok().value
            self.index += 1

            # insert array into symbol table to store dimension sizes
            ir.current_bb.symbol_table.insert_array(ident, dimensions)

            if self.tok().value == ",":
                # handle another designator
                self.index += 1
            elif self.tok().value == ";":
                # end of array declaration
                self.index += 1
                return

        self.error("Syntax Error in Array Declaration")

    def func_decl(self):
        signature = self.tok().value  # void vs func with return
        self.index += 1

        if signature == "void":
            if self.tok().value == "function":
                self.index += 1
            else:
                self.error("Expected 'function keyword")

        if self.tok().type != "Ident":
            self.error("Expected function identifier.")

        signature += f"-{self.tok().value}"  # function name
        self.index += 1

        # function body
        # create free floating basic block for array declaration and upcoming stat sequence
        ir.current_bb = ir.new_block()

        # formal param
        params = []
        if self.tok().value == "(":
            self.index += 1
            parameter_count = 0
            while self.tok().type == "Ident":
                params.append(self.tok().value)
                parameter_count += 1
                signature += f"-P"  # as we can have only ints as parameters, the signature requires only a marker
                ir.current_bb.symbol_table.insert(self.tok().value, f"'{parameter_count}'")
                self.index += 1
                if self.tok().value == ",":
                    self.index += 1
            if self.tok().value == ")":
                self.index += 1
            else:
                self.error("Formal Param missing closing bracket ')'")

        if self.tok().value == ";":
            self.index += 1
        else:
            self.error("Expected ';' after formal parameter definition.")

        # add block to function roots as signature is now complete
        ir.function_roots[signature] = ir.current_bb

        # func var declaration
        self.type_decl()

        # function sequence
        if self.tok().value == "{":
            self.index += 1
        else:
            self.error("Expected '{' after function type declaration.")

        self.stat_sequence(None)

        if ir.all_statements[-1].name != "return":
            self.error("Missing return statement at the end of function.")

        if self.tok().value == "}":
            self.index += 1
        else:
            self.error("Expected '}' after function type declaration.")

        # end of function
        if self.tok().value == ";":
            self.index += 1
            ir.current_bb = ir.root
        else:
            self.error("Expected ';' after function body.")

    def stat_sequence(self, current_join: ir.BasicBlock):
        while token_is_statement(self.tok()):
            self.statement(current_join)
            if self.tok().value == ";":
                # consume ; and repeat loop if next token is a statement
                self.index += 1

    def statement(self, current_join: ir.BasicBlock):
        if self.tok().value == "let":
            self.index += 1
            self.assignment(current_join)
        elif self.tok().value == "if":
            self.index += 1
            self.if_statement(current_join)
        elif self.tok().value == "while":
            self.index += 1
            self.while_statement(current_join)
        elif self.tok().value == "call":
            self.index += 1
            self.funcCall(from_statement=True)
        elif self.tok().value == "return":
            self.index += 1
            self.ret()
        else:
            self.error("Not yet implemented.")
        # todo funcCall, returnStatement

    def assignment(self, current_join: ir.BasicBlock):

        # ref is either a symbol table reference e.g '1: int' or a Computation of the array element offset
        is_array, ref, desig = self.designator_assignment()

        if self.tok().value == "<-":
            self.index += 1
            right = self.expression()

            if is_array:
                offset: Computation = ref
                base_address_instr = ir.append_instruction("add", "(R30)", f"({desig}'base)").count
                adda = ir.append_instruction("adda", str(offset), f"({base_address_instr})").count
                ir.append_instruction("store", str(right), f"({adda})")

            else:
                if right.type == ComputationType.LITERAL:
                    # add node representing constant value
                    ir.current_bb.symbol_table.insert(desig, ir.program_counter)
                    ir.append_instruction("Const", f"#{right.value}")
                else:
                    ir.current_bb.symbol_table.insert(desig, right.value)

                # add phi instruction to current join
                if current_join is not None:
                    # adjust PHI operands based on which side (then = left / else = right) the current bb runs
                    if ir.current_bb.block_type is ir.BlockType.ELSE:
                        left_operand = f"({ref})"
                        right_operand = f"({ir.current_bb.local_lookup(desig)})"
                    else:
                        # THEN and While DO
                        right_operand = f"({ref})"
                        left_operand = f"({ir.current_bb.local_lookup(desig)})"

                    ir.append_phi("Phi", left_operand, right_operand, desig, current_join)

    def designator_assignment(self):
        """ This designator is only called from assignments and behaves slightly differently,
            as it does not load arrays
        """
        if self.tok().type == "Ident":
            desig = self.tok().value
            self.index += 1

            # handle array designator
            if self.tok().value == "[":
                offset = self.calculate_array_offset(desig)
                return True, offset, desig

            else:
                # symbol table / normal variable
                # else:
                reference = ir.current_bb.deep_lookup(desig)
                return False, reference, desig

    def designator(self) -> (str, str):
        """ Gets and returns a reference belonging to a read designator as well as that designator """
        reference = "(REFERENCE ERROR)"
        desig = "(UNDEFINED)"
        if self.tok().type == "Ident":
            desig = self.tok().value
            self.index += 1

            # handle array designator
            if self.tok().value == "[":
                offset = self.calculate_array_offset(desig)

                # we now have an array and the correct address of accessing its element
                base_address_instr = ir.append_instruction("add", "(R30)", f"({desig}'base)").count
                adda = ir.append_instruction("adda", str(offset), f"({base_address_instr})")
                reference = ir.append_instruction("load", f"({adda.count})").count

            else:
                # symbol table
                reference = ir.current_bb.deep_lookup(desig)

        return reference, desig

    def consume_array(self):
        """ Parses the tokens belonging to an array
            Namely curly braces and expressions. The Expressions are saved as Computations in a list and returned
        """
        dimensions: [Computation] = []
        while self.tok().value == "[":
            self.index += 1
            one_expression = self.expression()
            dimensions.append(one_expression)
            if self.tok().value == "]":
                self.index += 1
            else:
                self.error("Missing ']' bracket in designator.")
        return dimensions

    def calculate_array_offset(self, desig) -> Computation:
        """Traverses the array with the given designator and calculates the offset position (*4 included)"""
        # list of access indices
        fields: [Computation] = self.consume_array()

        # calculate memory offset
        array_entry = ir.current_bb.array_lookup(desig)
        if array_entry is None:
            self.error("Use of undeclared array!")
        arr_dim_sizes = array_entry[1]
        d = len(arr_dim_sizes)
        offset = Computation(ComputationType.LITERAL, 0)

        i = d - 1
        while i >= 0:
            j = d - 1
            prod = Computation(ComputationType.LITERAL, 1)
            while j >= i + 1:
                N_j = Computation(ComputationType.LITERAL, arr_dim_sizes[j])
                prod = compute("mul", prod, N_j)
                j -= 1
            field: Computation = fields[i]
            prod = compute("mul", prod, field)
            offset = compute("add", offset, prod)
            i -= 1

        offset = compute("mul", offset, Computation(ComputationType.LITERAL, 4))

        return offset

    def if_statement(self, outer_join: ir.BasicBlock):
        relop, pc_cmp = self.relation()
        current_block = ir.current_bb
        join_block = ir.new_block(current_block)

        # then keyword
        if self.tok().value == "then":
            self.index += 1
            # stat sequence in new basic block
            then_block = ir.new_block(current_block)
            then_block.block_type = ir.BlockType.THEN
            current_block.fallthrough = then_block
            ir.current_bb = then_block
            self.stat_sequence(join_block)
            then_bra_instr = ir.append_instruction("bra", f"(UNDEF)")
            ir.current_bb.branch = join_block
            ir.current_bb = join_block
        else:
            self.error("Expected 'then' keyword")

        # optional else
        else_block = None
        if self.tok().value == "else":
            self.index += 1
            else_block = ir.new_block(current_block)
            else_block.block_type = ir.BlockType.ELSE
            current_block.branch = else_block
            ir.current_bb = else_block
            self.stat_sequence(join_block)
            ir.current_bb.fallthrough = join_block
            ir.current_bb = join_block
        else:
            # no else block, add join as branch to current block
            current_block.branch = join_block

        # if delimiter
        if self.tok().value == "fi":
            self.index += 1

            # update join block instruction search structure as arrays might have been assigned
            join_block.search_structure = current_block.search_structure.copy()

            # add branching statements to correct blocks
            # find join instruction
            if len(join_block.instructions) > 0:
                pc_join = join_block.instructions[0].count
            else:
                # assume the next instruction will be one added to the join
                pc_join = ir.program_counter + 1

            # get else first instruction or join instruction
            if else_block is not None and len(else_block.instructions) > 0:
                cmp_branch_instr = else_block.instructions[0].count
            else:
                # branch to the instruction next populating the join block
                cmp_branch_instr = pc_join

            # append branch after comparison
            ir.append_instruction(relop, f"({pc_cmp})", f"({cmp_branch_instr})", basic_block=current_block)

            # update branch instruction counter
            then_bra_instr.a = f"({pc_join})"

            # phi functions
            if outer_join is not None:
                join_block.branch = outer_join
                # for every phi function in the current join, propagate the new value to outer join block
                for instr in join_block.instructions:
                    if isinstance(instr, ir.PhiOperation):
                        # this should always update an existing function so we do not care about left-right operators
                        operand = f"({ir.current_bb.local_lookup(instr.symbol)})"
                        ir.append_phi("Phi", operand, operand, instr.symbol, outer_join)

        else:
            self.error("Expected 'fi' delimiter at end of if statement.")

    def while_statement(self, outer_join: ir.BasicBlock):
        # create the join block for the top of the loop
        join_block = ir.new_block(ir.current_bb)
        ir.current_bb.fallthrough = join_block
        ir.current_bb = join_block

        # the domination can not be guaranteed for join blocks
        # for now "forget" all dominating expressions
        # todo somehow check if an assignment happens to a modified variable in another pass?
        join_block.search_structure = ir.SearchStructure()

        # take note of the program count, as instructions will need to be updated - done in pc_cmp
        # add the relation
        relop, pc_cmp = self.relation()

        # do keyword
        if self.tok().value == "do":
            self.index += 1
        else:
            self.error("Expected 'do' keyword")

        # do block
        do_block = ir.new_block(join_block)
        do_block.block_type = ir.BlockType.THEN  # do is equivalent to then
        join_block.fallthrough = do_block
        ir.current_bb = do_block

        # inner statements
        self.stat_sequence(join_block)

        # reorder join block so that PHI is above cmp
        cmp_instr = None
        for i in reversed(ir.all_statements):
            if i.count == pc_cmp:
                cmp_instr = i
        join_block.instructions.remove(cmp_instr)
        join_block.instructions.append(cmp_instr)

        # add loop back
        ir.current_bb.branch = join_block
        branch_loop_instr = ir.append_instruction("bra", f"({join_block.instructions[0].count})")

        # UPDATE ALL statements that contain the right side of a phi statement as an operand
        for instr in join_block.instructions:
            if isinstance(instr, ir.PhiOperation):
                new_value = f"({instr.count})"
                old_value = instr.b
                for victim in ir.all_statements[pc_cmp - 1:]:
                    if isinstance(victim, ir.PhiOperation):
                        if instr == victim:
                            continue
                        # update only left side
                        if victim.a == old_value:
                            victim.a = new_value  # instr.b
                        if victim.a == instr.a:
                            victim.a = new_value

                    else:
                        # update right and left side
                        if victim.a == old_value:
                            victim.a = new_value
                        if victim.b == old_value:
                            victim.b = new_value

        # branching statement to next block
        next_block = ir.new_block(join_block)
        join_block.branch = next_block
        # append branch after comparison
        ir.current_bb = next_block
        ir.append_instruction(relop, f"({pc_cmp})", f"({ir.program_counter + 1})", basic_block=join_block)

        # do delimiter
        if self.tok().value == "od":
            self.index += 1
            # for every phi function in the current join, propagate the new value to outer join block
            if outer_join is not None:
                next_block.branch = outer_join
                for instr in join_block.instructions:
                    if isinstance(instr, ir.PhiOperation):
                        # this should always update an existing function so we do not care about left-right operands
                        operand = f"({instr.count})"
                        ir.append_phi("Phi", operand, operand, instr.symbol, outer_join)
        else:
            self.error("Expected 'od' delimiter at end of while statement.")

    def relation(self):
        """
        Compares and generates a Result
        0 if Relation is equal, 1 if left hand side is greater, -1 if left hand side is smaller
        returns the correct branching opcode and the program count to be referenced
        """
        # first expression
        left = self.expression()

        # relOp
        if self.tok().type == "RelOp":
            relop = self.tok().value
            self.index += 1
        else:
            self.error("Expected relational Operator")

        # the other expression
        right = self.expression()

        # note the program count of compare instruction to reference in branch statement
        pc_cmp = ir.program_counter
        # generate instructions - compare
        ir.append_instruction("cmp", str(left), str(right))

        # determine branching operation / reverse logic because then is fall through
        if relop == "==":
            op = "beq"
        elif relop == "!=":
            op = "bne"
        elif relop == "<=":
            op = "bgt"
        elif relop == ">=":
            op = "blt"
        elif relop == "<":
            op = "bge"
        elif relop == ">":
            op = "ble"

        return op, pc_cmp

    def expression(self) -> Computation:
        left = self.term()
        while self.tok().value == "+" or self.tok().value == "-":
            if self.tok().value == "+":
                op = "add"
            else:
                op = "sub"
            self.index += 1
            right = self.term()

            left = compute(op, left, right)

        return left

    def term(self) -> Computation:
        left = self.factor()
        while self.tok().value == "*" or self.tok().value == "/":
            if self.tok().value == "*":
                op = "mul"  # multiplication
            else:
                op = "div"  # division
            self.index += 1
            right = self.factor()

            left = compute(op, left, right)

        return left

    def factor(self) -> Computation:
        result = Computation()

        if self.tok().type == "Ident":
            reference, _ = self.designator()
            result.type = ComputationType.VAR
            result.value = reference
            return result

        elif self.tok().type == "Number":
            result.type = ComputationType.LITERAL
            result.value = self.tok().value
            self.index += 1
            return result

        elif self.tok().value == "(":
            self.index += 1
            result = self.expression()
            if self.tok().value != ")":
                self.error("Missing ')' around Expression")
            else:
                self.index += 1
                return result

        elif self.tok().value == "call":
            self.index += 1
            f_result = self.funcCall(from_statement=False)
            if f_result is None:
                self.error("Only non-void functions may be used in Expressions.")
            return f_result
        else:
            self.error("Unexpected Symbol in Factor")

    def funcCall(self, from_statement=False) -> Computation:
        if self.tok().type != "Ident":
            self.error("Expected ident after 'call' keyword.")

        def consume_redundant_parenthesis():
            if self.tok().value == "(":
                self.index += 1
                if self.tok().value == ")":
                    self.index += 1
                else:
                    self.error("Redundant Parenthesis opened but did not close.")

        # check for inbuilts
        ident = self.tok().value
        self.index += 1
        if ident == "InputNum":
            consume_redundant_parenthesis()
            ir.append_instruction("read")
            result = Computation()
            result.type = ComputationType.VAR
            result.value = ir.program_counter - 1  # read instruction number
            return result

        elif ident == "OutputNewLine":
            consume_redundant_parenthesis()
            ir.append_instruction("writeNL")

        elif ident == "OutputNum":
            if self.tok().value != "(":
                self.error("Expected opening parenthesis.")
            self.index += 1

            x = self.expression()
            ir.append_instruction("write", str(x))

            if self.tok().value != ")":
                self.error("Expected closing parenthesis.")
            self.index += 1

        else:
            # user defined functions
            if from_statement:
                signature = f"void-{ident}"
            else:
                signature = f"function-{ident}"

            # consume parameters to determine complete signature
            params = []
            if self.tok().value == "(":
                self.index += 1

                while self.tok().value != ")":
                    expr = self.expression()
                    params.append(expr)
                    signature += "-P"
                    if self.tok().value == ",":
                        self.index += 1
                        continue

                if self.tok().value == ")":
                    self.index += 1
                else:
                    self.error("Function call missing closing parenthesis.")

            # try to find function with signature
            function_block: ir.BasicBlock = ir.function_roots[signature]
            if function_block is None:
                self.error("No Function defined for given signature.")

            # add param statements
            for param in params:
                ir.append_instruction("param", str(param))

            # call to function
            call = ir.append_call("call", f"{ident}", f"{len(params)}", function_block)

            # return value
            if from_statement:
                return None
            else:
                return Computation(ComputationType.VAR, call.count)

    def ret(self):
        """ Return statement """
        if self.tok().value in ";}":
            ir.append_instruction("return")
        else:
            x = self.expression()
            ir.append_instruction("return", str(x))


def token_is_statement(t: Token):
    """Returns true if token is start to a statement"""
    if t.value == "let" or t.value == "call" or t.value == "if" or t.value == "while" or t.value == "return":
        return True
    else:
        return False


def compute(op, a: Computation, b: Computation) -> Computation:

    if op == "add":
        # check if we can directly calculate the sub product
        if a.type == b.type == ComputationType.LITERAL:
            a.value = int(a.value) + int(b.value)
        elif a.type == ComputationType.LITERAL and a.value == 0:
            # neutral element
            a = b
        elif b.type == ComputationType.LITERAL and b.value == 0:
            # neutral element
            pass
        else:
            a.value = ir.append_instruction("add", str(a), str(b)).count
            a.type = ComputationType.VAR

    if op == "sub":
        # check if we can directly calculate the sub product
        if a.type == b.type == ComputationType.LITERAL:
            a.value = int(a.value) - int(b.value)
        elif a.type == ComputationType.LITERAL and a.value == 0:
            # neutral element
            a = b
        elif b.type == ComputationType.LITERAL and b.value == 0:
            # neutral element
            pass
        else:
            a.value = ir.append_instruction("sub", str(a), str(b)).count
            a.type = ComputationType.VAR

    elif op == "mul":
        # check if we can directly calculate the sub product
        if a.type == b.type == ComputationType.LITERAL:
            a.value = int(a.value) * int(b.value)
        elif a.type == ComputationType.LITERAL and a.value == 1:
            # neutral element
            a = b
        elif b.type == ComputationType.LITERAL and b.value == 1:
            # neutral element
            pass
        else:
            a.value = ir.append_instruction("mul", str(a), str(b)).count
            a.type = ComputationType.VAR

    if op == "div":
        # check if we can directly calculate the sub product
        if a.type == b.type == ComputationType.LITERAL:
            a.value = int(a.value) // int(b.value)
        elif a.type == ComputationType.LITERAL and a.value == 1:
            # neutral element
            a = b
        elif b.type == ComputationType.LITERAL and b.value == 1:
            # neutral element
            pass
        else:
            a.value = ir.append_instruction("div", str(a), str(b)).count
            a.type = ComputationType.VAR

    return a
    # todo sub div
