class EasyTable:
    def __init__(self):
        self.table: [(str, str)] = []
        self.arrays: [(str, [int])] = []

    def insert(self, name: str, instruction: str):
        self.table.append((name, instruction))
        return instruction

    def lookup(self, name: str):
        existing_sym = None

        for sym in self.table:
            if name == sym[0]:
                existing_sym = sym

        if existing_sym is not None:
            return existing_sym[1]
        else:
            return None

    def insert_array(self, name: str, dimensions: [int]):
        self.arrays.append((name, dimensions))

    def lookup_array(self, name):
        for entry in self.arrays:
            if entry[0] == name:
                return entry
