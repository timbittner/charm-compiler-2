""" Module describing the parse tree """
from enum import Enum
from IntermediateRepresentation.Symboltable import EasyTable


class Operation:
    """ Models an operation
        UPPERCASE name indicates DLX ready instruction,
    """

    def __init__(self, program_count: int, name: str, a: str = None, b: str = None):
        self.count = program_count
        self.name = name
        self.a = a
        self.b = b

    def __copy__(self):
        return Operation(self.count, self.name, self.a, self.b)

    def __str__(self):
        a = "" if self.a is None else self.a
        b = "" if self.b is None else self.b
        return f"{self.count}: {self.name :>5} {a :>3} {b :>3}"

    def __eq__(self, other):
        """Two operations are equal if they have the same instruction-name and operands"""
        if isinstance(other, Operation):
            # Instruction name matches
            if self.name == other.name:
                # operands a
                if self.a is not None and other.a is not None:
                    if self.a != other.a:
                        return False
                # operands b
                if self.b is not None and other.b is not None:
                    if self.b != other.b:
                        return False
                # both operands are equal if they exist from here on
                return True

        return False


class PhiOperation(Operation):
    """ Models a phi operation that holds the identifier it modifes """

    def __init__(self, program_count: int, name: str, left, right, symbol):
        super().__init__(program_count, name, left, right)
        self.symbol: str = symbol

    def __copy__(self):
        return PhiOperation(self.count, self.name, self.a, self.b, self.symbol)


class CallOperation(Operation):
    def __init__(self, program_count: int, name: str, left, right, function_block: 'BasicBlock'):
        super().__init__(program_count, name, left, right)
        self.function_block: 'BasicBlock' = function_block

    def __copy__(self):
        return CallOperation(self.count, self.name, self.a, self.b, self.function_block)


class SearchStructure:
    def __init__(self):
        self.add: [Operation] = []
        self.sub: [Operation] = []
        self.mul: [Operation] = []
        self.div: [Operation] = []
        self.cmp: [Operation] = []
        self.adda: [Operation] = []
        self.mem: [Operation] = []  # load and store

    def append_if_uncommon(self, op: Operation):
        """ Checks if a given operation is a common subexpression.
            Appends new unique instructions to class lists
            Returns a the Existing Operation if a dominating expression is found
             or None if the given op ist unique.
            Returns the false and the parameter op for all instructions that are untracked.
            See __init__ for tracked instructions.
        """
        def check_unique(op: Operation, l: [Operation]) -> (bool, Operation):
            """ Returns true and None if an operation op is not contained in the given list l
                Otherwise returns False and the matching existing instruction
            """
            for existing_instr in reversed(l):
                if op == existing_instr:
                    return False, existing_instr
            return True, None

        if op.name == "add":
            unique, existing_instr = check_unique(op, self.add)
            if unique:
                self.add.append(op)
            else:
                return existing_instr

        elif op.name == "sub":
            unique, existing_instr = check_unique(op, self.sub)
            if unique:
                self.sub.append(op)
            else:
                return existing_instr

        elif op.name == "mul":
            unique, existing_instr = check_unique(op, self.mul)
            if unique:
                self.mul.append(op)
            else:
                return existing_instr

        elif op.name == "div":
            unique, existing_instr = check_unique(op, self.div)
            if unique:
                self.div.append(op)
            else:
                return existing_instr

        elif op.name == "cmp":
            unique, existing_instr = check_unique(op, self.cmp)
            if unique:
                self.cmp.append(op)
            else:
                return existing_instr

        elif op.name == "adda":
            unique, existing_instr = check_unique(op, self.adda)
            if unique:
                self.adda.append(op)
            else:
                return existing_instr

        elif op.name == "load":
            unique, existing_instr = check_unique(op, self.mem)
            if unique:
                self.mem.append(op)
            else:
                return existing_instr

        # return None if the operation is a common subexpression
        # don't to anything to instructions that do not fall into notable lists
        return None

    def copy(self) -> 'SearchStructure':
        copy = SearchStructure()
        for op in self.add:
            copy.add.append(op)
        for op in self.sub:
            copy.sub.append(op)
        for op in self.mul:
            copy.mul.append(op)
        for op in self.div:
            copy.div.append(op)
        for op in self.cmp:
            copy.cmp.append(op)
        for op in self.adda:
            copy.adda.append(op)
        for op in self.mem:
            copy.mem.append(op)
        return copy


class BlockType(Enum):
    DEFAULT = 1
    THEN = 2
    ELSE = 3


class BasicBlock:
    def __init__(self, block_id: int):
        self.instructions: [Operation] = []
        self.search_structure = SearchStructure()
        self.symbol_table = EasyTable()
        self.block_id = block_id
        self.block_type: BlockType = BlockType.DEFAULT
        self.dominator_list: [BasicBlock] = []
        self.fallthrough: BasicBlock = None
        self.branch: BasicBlock = None
        self.parent: BasicBlock = None

    def local_lookup(self, name: str):
        return self.symbol_table.lookup(name)

    def deep_lookup(self, name: str):
        sym = self.local_lookup(name)
        if sym is None and self.parent is not None:
            sym = self.parent.deep_lookup(name)
        return sym

    def array_lookup(self, name: str):
        entry = self.symbol_table.lookup_array(name)
        if entry is None:
            entry = self.parent.symbol_table.lookup_array(name)
        return entry

    def copy_symbol_table(self, other_table: EasyTable):
        self.symbol_table = EasyTable()
        for entry in other_table.table:
            self.symbol_table.table.append(entry)
        for entry in other_table.arrays:
            self.symbol_table.arrays.append(entry)

    def forget_upstream_memory(self):
        self.search_structure.mem  = []
        self.search_structure.adda = []
        if self.parent is not None:
            self.parent.forget_upstream_memory()


# The graph representing the program flow - Singleton
next_block_id = 1
root = BasicBlock(next_block_id)  # auto append the first building block
current_bb = root
program_counter = 1
all_statements: [Operation] = []
function_roots = {}


def reset():
    """ Resets this modules' variables """
    global next_block_id, root, current_bb, program_counter, all_statements, function_roots
    next_block_id = 1
    root = BasicBlock(next_block_id)  # auto append the first building block
    current_bb = root
    program_counter = 1
    all_statements = []
    function_roots = {}


def new_block(parent: BasicBlock = None):
    global next_block_id
    next_block_id += 1
    b = BasicBlock(next_block_id)
    if parent is not None:
        b.block_type = parent.block_type
        b.copy_symbol_table(parent.symbol_table)
        b.search_structure = parent.search_structure.copy()
        b.parent = parent
        parent.dominator_list.append(b)
    return b


def print_repr():
    """ Prints the current intermediate representation to console using recursive descent and root"""
    printed_blocks = []
    bb = root

    def print_instr(block):
        for instr in block.instructions:
            print(f" {instr}")

    def print_table(block: BasicBlock):
        print(f"\tTABLE: ")
        for entry in block.symbol_table.table:
            print(f"\t {entry[0]} {entry[1]}")

    def print_next(b: BasicBlock):
        printed_blocks.append(b.block_id)
        print(f"BB:{b.block_id}")
        print_instr(b)
        print_table(b)
        blocks = [b.fallthrough, b.branch]
        for nb in blocks:
            if nb is not None and nb.block_id not in printed_blocks:
                print_next(nb)

    print_next(bb)


def append_instruction(operation: str, a: str = None, b: str = None, basic_block: BasicBlock = None):
    if a is not None and not isinstance(a, str):
        raise RuntimeError("Invalid Operand a")

    if b is not None and not isinstance(b, str):
        raise RuntimeError("Invalid Operand b")

    global program_counter
    instruction = Operation(program_counter, operation, a, b)

    if basic_block is None:
        # append to current
        basic_block = current_bb

    # delete all upstream memory lists when appending a store
    if operation == "store":
        basic_block.forget_upstream_memory()

    # check for common subexpressions
    dom_instr = basic_block.search_structure.append_if_uncommon(instruction)
    if dom_instr is None:
        # new unique instruction
        basic_block.instructions.append(instruction)
        all_statements.append(instruction)
        program_counter += 1
        return instruction
    else:
        # existing instruction. No PC increment. No appending.
        return dom_instr


def append_phi(operation: str, left, right, symbol, basic_block: BasicBlock):
    global program_counter

    # is there already a phi function modifying the current symbol?
    existing_phi = None
    for instr in basic_block.instructions:
        if isinstance(instr, PhiOperation):
            if instr.symbol == symbol:
                existing_phi = instr

    if existing_phi is None:
        # just create a new phi and insert symbol into table
        phi_instr = PhiOperation(program_counter, operation, left, right, symbol)
        basic_block.instructions.append(phi_instr)
        all_statements.append(phi_instr)
        basic_block.symbol_table.insert(symbol, program_counter)
        program_counter += 1
    else:
        # update the phi function on the correct side, do NOT increase program counter
        if current_bb.block_type == BlockType.ELSE:
            # modify right side only
            existing_phi.b = right
        else:
            # modify left side only
            existing_phi.a = left


def append_call(operation: str, a: str, b: str, function_block: BasicBlock, basic_block: BasicBlock = None):
    if a is not None and not isinstance(a, str):
        raise RuntimeError("Invalid Operand a")

    if b is not None and not isinstance(b, str):
        raise RuntimeError("Invalid Operand b")

    global program_counter
    instruction = CallOperation(program_counter, operation, a, b, function_block)

    if basic_block is None:
        # append to current
        basic_block = current_bb

    # calls are always unique
    basic_block.instructions.append(instruction)
    all_statements.append(instruction)
    program_counter += 1
    return instruction
